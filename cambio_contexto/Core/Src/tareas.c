#include "tareas.h"
#include "main.h"
#include <string.h>

cambia_contexto_tarea	*actual, *futura;
static cambia_contexto_tarea 	lista_tareas[MAX_TAREAS];
static uint32_t	ntarea, max_tarea;

void cambia_contexto_agregar_tarea(void (*tarea)(void), uint32_t slices, void *stack, uint32_t lenstack_words)
{
	uint32_t i;
	uint32_t *ptr = (uint32_t*)stack;

	/*
	 * Esta función genera el stack para cambiar el "contexto" del procesador para el primer cambio
	 * Para ayudarme use el link:
	 * https://developer.arm.com/documentation/dui0552/a/the-cortex-m3-processor/exception-model/exception-entry-and-return
	 *
	 * Cada tarea tiene una cantidad de ticks que va a ejecutarse, su propia pila y datos
	 * que no se almacenan en la pila que se guardan aparte.
	 */

	for(i=0;i<MAX_TAREAS-1;i++)
	{
		if(!lista_tareas[i].slices)
		{
			max_tarea++;														//Incremento la cantidad de tareas.
			lista_tareas[i].slices  = slices;									//Cuantos "ticks" le voy a dedicar
			lista_tareas[i].ticks   = slices;									//Arranca con todos los ticks.
			lista_tareas[i].pila    = ((uint32_t)ptr)+((lenstack_words-8)<<2);	//Cada tarea tiene su propia pila, uso un espacio de 8 registros de 32 bits por el stacking.
			*(ptr+lenstack_words-1)	= 1<<24;									//El xPSR debe tener el bit T (Thumb) en 1.
			*(ptr+lenstack_words-2)	= (uint32_t)tarea;							//PC a restaurar, lo apunto a la tarea que quiero.
			*(ptr+lenstack_words-3)	= 0;										//LR. El link register, la primera vez no importa.
			*(ptr+lenstack_words-4)	= 0;										//R12. La primera vez no me importa lo que vale.
			*(ptr+lenstack_words-5)	= 0;										//R3
			*(ptr+lenstack_words-6)	= 0;										//R2
			*(ptr+lenstack_words-7)	= 0;										//R1
			*(ptr+lenstack_words-8)	= 0;										//R0
			break;
		}
	}
}

static void cambiar_tareas(cambia_contexto_tarea *t_actual, cambia_contexto_tarea *t_futura)
{
		actual = t_actual;
		futura = t_futura;
		if(actual != futura)
		{
			/*
			 * Las tareas se cambian cuando se ejecuta
			 * la excepción asociada a la Pend_SV.
			 *
			 * Si necesito cambiar la tarea pido la Pend_SV
			 */
			trigger_pendSV();

			/*
			 * Me fijo si estoy corriendo en una excepción
			 * o no. Si estoy en excepción no hago nada. Si
			 * no estoy en excepción llamo a la SVC, que no
			 * va a hacer nada, pero me va a forzar la aparición
			 * de la pendSV();
			 */
			if(!ami_in_exception())
			{
				call_svc();
			}
		}
}

void cambia_contexto_correr_tareas(void)
{
	static cambia_contexto_tarea dummy;
	static uint8_t pila_dummy[64];
	dummy.pila = ((uint32_t)pila_dummy)+64;
	cambiar_tareas(&dummy,&lista_tareas[0]);
}

void cambia_contexto_despachar_tareas(void)
{
	cambia_contexto_tarea *t_actual;
	if(!--lista_tareas[ntarea].ticks)
	{
		lista_tareas[ntarea].ticks = lista_tareas[ntarea].slices;
		t_actual = &lista_tareas[ntarea];
		if(++ntarea>=max_tarea) ntarea=0;
		cambiar_tareas(t_actual,&lista_tareas[ntarea]);
	}
}

void cambia_contexto_tarea_siguiente(void)
{
	cambia_contexto_tarea *t_actual;
	lista_tareas[ntarea].ticks = lista_tareas[ntarea].slices;
	t_actual = &lista_tareas[ntarea];
	if(++ntarea>=max_tarea) ntarea=0;
	cambiar_tareas(t_actual,&lista_tareas[ntarea]);
}

void cambia_contexto_inicializar_tareas(void)
{
	memset(lista_tareas,0,MAX_TAREAS*sizeof(cambia_contexto_tarea));
	actual = &lista_tareas[0];
	futura = &lista_tareas[0];
	ntarea = 0;
	max_tarea = 0;
}
