#ifndef SRC_TAREAS_H_
#define SRC_TAREAS_H_
#define	MAX_TAREAS	32
#include <stdint.h>
#include <main.h>
#define trigger_pendSV() 		{SCB->ICSR|= SCB_ICSR_PENDSVSET_Msk;}
#define call_svc()				{__asm volatile("svc 0"); }
#define ami_in_exception() 		(__get_IPSR())

typedef struct
{
	uint32_t	registros[8];	//Los registros que no guardo en la pila los guardo acá R4 - R11
	uint32_t	pila;			//Pila de la tarea, de acá uso 32 bytes para el stacking
	uint32_t	ticks;			//ticks que le restan a la tarea para irse.
	uint32_t	slices;			//cantidad de ticks que corre la tarea.
} cambia_contexto_tarea;

void cambia_contexto_agregar_tarea(void (*tarea)(void), uint32_t slices, void *stack, uint32_t lenstack_words);
void cambia_contexto_correr_tareas(void);
void cambia_contexto_despachar_tareas(void);
void cambia_contexto_inicializar_tareas(void);
void cambia_contexto_tarea_siguiente(void);


#endif /* SRC_TAREAS_H_ */
